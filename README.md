# Transliteration

A module to transliterate Indian languages.

A near port of the [Python libindic transliterator](https://github.com/libindic/Transliteration/).

* Malayalam : Uses [ml2en library](https://github.com/knadh/ml2en/)
* Hindi : Uses libindic algorithm
* Kannada : Uses libindic algorithm

This module is used in the WebExtension [Indic-En](//subinsb.com/indicen/).